Readme file for the Icon span Injector filter module
-----------------------------------------------------

Icon span Injector filter.module is a filter which add css based icons to links by injecting &lt;span&gt; tags 
into &lt;a&gt; tags in the filtered content.



Installation:
  Installation is like with all normal drupal modules:
  extract the 'icon_span_injector_filter' folder from the tar ball to the
  modules directory from your website (typically sites/all/modules).

Dependencies:
  The Icon span Injector filter module has no dependencies, nothing special is required.